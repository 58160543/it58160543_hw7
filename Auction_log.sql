drop table if exists Auction_log;

CREATE TABLE Auction_log
(
	Action		VARCHAR(10) NOT NULL,
	AUC_ID		INT  NOT NULL,
	TimeStamp	DATETIME NOT NULL,
	PROD_ID		INT NOT NULL,
	Bid		INT NOT NULL
)Engine=Innodb;
