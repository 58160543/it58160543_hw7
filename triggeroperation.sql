Drop trigger if exists triggeroperation;

delimiter $$

create trigger triggeroperation
        AFTER insert on Auction_log
        for each row

begin
         if(NEW.Action = 'create') then
                insert Auction values (NEW.AUC_ID,NEW.TimeStamp,NEW.PROD_ID,NEW.Bid);

        end if;

        if(NEW.Action = 'update')   then
                update Auction set
                Bid = NEW.Bid
                where AUC_ID = NEW.AUC_ID;
 	end if;

        if(NEW.Action = 'delete') then
                delete from Auction
                where AUC_ID = NEW.AUC_ID;

        end if;

end $$

delimiter ;
